<?php
namespace Riddlemd\Taggable\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Riddlemd\Tools\Utility\IO;
use Cake\Console\ConsoleOptionParser;

class CalculateTagCountCommand extends Command
{
    protected function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser->addArgument('tag_id', [
            'help' => 'What id of tag that should be counted'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
        $this->loadModel('Riddlemd/Taggable.Tags');

        $tagId = $args->getArgument('tag_id') ?? null;

        $tags = $tagId ? $this->Tags->findById((int)$tagId) : $this->Tags->find();

        foreach($tags as $tag)
        {
            $io->out("Calculating count for `{$tag->name}` with context `{$tag->fk_table}`... ", false);
            $count = $this->Tags->CalculateCount($tag);
            $io->out($count);
        }

        $io->out('Done!');
    }
}