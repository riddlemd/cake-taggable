<?php
namespace Riddlemd\Taggable\Model\Table;

use Cake\ORM\Table;
use Riddlemd\Tools\Core\Configure;

class TagAssociationsTable extends Table
{
    public function initialize(array $config)
    {
        $this->setTable('taggable__tag_associations');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Tags', [
            'className' => 'Riddlemd/Taggable.Tags',
            'foreignKey' => 'tag_id'
        ]);
    }
}