<?php
namespace Riddlemd\Taggable\Model\Table;

use Cake\ORM\Table;
use Riddlemd\Taggable\Model\Entity\Tag;
use Riddlemd\Tools\Core\Configure;

class TagsTable extends Table
{
    public function initialize(array $config)
    {
        $this->setTable('taggable__tags');
        $this->addBehavior('Timestamp');

        $this->hasMany('TagAssociations', [
            'className' => 'Riddlemd/Taggable.TagAssociations',
            'foreignKey' => 'tag_id'
        ]);
    }

    public function CalculateCount(Tag $tag)
    {
        $tag->count = $this->TagAssociations->findByTagId($tag->id)->count();
        $this->save($tag);
        return $tag->count;
    }
}