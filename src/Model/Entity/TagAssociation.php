<?php
namespace Riddlemd\Taggable\Model\Entity;

use Cake\ORM\Entity;

class TagAssociation extends Entity
{
    public $accessible = [
        '*' => false,
        'label' => true,
    ];
}