<?php
namespace Riddlemd\Taggable\Model\Entity;

use Cake\Core\Configure;

trait TaggableTrait
{

    public function _getTags()
    {
        if(empty($this->tag_associations) || !is_array($this->tag_associations)) return '';

        $tags = [];
        $delimiter = Configure::read('Riddlemd/Taggable.delimiter');
        foreach($this->tag_associations as $tagAssocation)
        {
            if(is_string($tagAssocation))
            {
                $tags[] = $tagAssocation;
            }
            else
            {
                if(empty($tagAssocation->tag)) continue;
                $tags[] = $tagAssocation->tag->name;
            }
        }

        return implode($delimiter, $tags);
    }

    public function _setTags($tags)
    {
        if(empty($this->tag_associations) || !is_array($this->tag_associations)) $this->tag_associations = [];

        $delimiter = trim(Configure::read('Riddlemd/Taggable.delimiter'));
        if(is_string($tags)) $tags = explode($delimiter, $tags);

        // Filter out removed associations
        $this->tag_associations = array_filter($this->tag_associations, function($tagAssociation) use (&$tags) {
            $tag = is_string($tagAssociation) ? $tagAssocation : (empty($tagAssocation->tag) ? $tagAssociation->tag->name : null);
            if($tag)
            {
                if(in_array($tag, $tags))
                {
                    unset($tags[array_search($tag, $tags)]);
                    return true;
                }
            }
            return false;
        });

        foreach($tags as $tag) {
            $this->tag_associations[] = trim($tag);
        }
    }
}