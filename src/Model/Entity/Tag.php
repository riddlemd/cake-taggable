<?php
namespace Riddlemd\Taggable\Model\Entity;

use Cake\ORM\Entity;

class Tag extends Entity
{
    public $accessible = [
        '*' => false,
        'label' => true,
    ];
}