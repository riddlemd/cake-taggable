<?php
namespace Riddlemd\Taggable\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;
use Riddlemd\Tools\Core\Configure;

class TaggableBehavior extends Behavior
{
    protected $_tagCache = [];

    public function initialize(array $config = [])
    {
        $this->setConfig(array_merge(Configure::read('Riddlemd/Taggable'), $config));

        $table = $this->_table;

        if (!$table->hasAssociation('TagAssociations')) {
            $table->hasMany('TagAssociations', [
                'className' => 'Riddlemd/Taggable.TagAssociations',
                'foreignKey' => 'fk_id',
                'conditions' => [
                    'TagAssociations.fk_table' => $table->getTable()
                ],
                'saveStrategy' => 'replace'
            ]);
        }
    }

    public function filterQueryByKeywords(Query $query, $keywords)
    {
        if(is_string($keywords)) $keywords = explode(' ', $keywords);

        $query->matching('TagAssociations.Tags', function($q) use($keywords) {
            return $q->where([
                'Tags.name IN' => $keywords
            ]);
        });
    }

    public function beforeFind($event, $query)
    {
        $query->contain([
            'TagAssociations',
            'TagAssociations.Tags'
        ]);
    }


    public function beforeSave(Event $event, Entity $entity)
    {
        $eventData = $event->getData();

        if(empty($eventData['options']['associated'])) $eventData['options']['associated'] = [];
        $eventData['options']['associated']['TagAssociations'] = [];
        $event->setData($eventData);

        if(is_array($entity->tag_associations ?? null))
        {
            foreach($entity->tag_associations as $tagAssociationKey => &$tagAssociation)
            {
                if(empty($tagAssociation))
                {
                    unset($entity->tag_associations[$tagAssociationKey]);
                    continue;
                }

                if(is_string($tagAssociation))
                {
                    $tag = $this->getTagEntity($tagAssociation);
                    $tagAssociation = $this->createTagAssociationEntity($entity, $tag);
                }
            }
        }
    }

    protected function createTagAssociationEntity(Entity $entity, Entity $tag) : Entity
    {
        $table = $this->_table;

        if(is_array($entity->tag_associations))
        {
            foreach($entity->tag_associations as $tagAssoc)
            {
                if(!is_string($tagAssoc))
                {
                    if($tagAssoc->tag_id == $tag->id)
                    {
                        return $tagAssoc;
                    }
                }
            }
        }
        else
        {
            $entity->tag_associations = [];
        }

        $tagAssoc = $table->TagAssociations->newEntity();
        $tagAssoc->tag_id = $tag->id;
        $tagAssoc->fk_id = $entity->id;

        $entityTable = TableRegistry::get($entity->getSource());

        $tagAssoc->fk_table = $entityTable->getTable();

        return $tagAssoc;
    }

    protected function getTagEntity(String $tagName) : ?Entity
    {
        $tagName = $this->normalizeTagName($tagName);
        if(!empty($tagName))
        {
            if(!empty($this->_tagCache[$tagName]))
            {
                return $this->_tagCache[$tagName];
            }

            $table = $this->_table;

            $tag = $table->TagAssociations->Tags->find()
                ->where([
                    'Tags.name' => $tagName,
                    'Tags.fk_table' => $table->getTable()
                ])
                ->first();

            if(empty($tag))
            {
                $tag = $table->TagAssociations->Tags->newEntity();
                $tag->name = $tagName;
                $tag->fk_table = $table->getTable();

                if(!$table->TagAssociations->Tags->save($tag))
                {
                    throw \Riddlemd\Taggable\Exception\CouldNotSaveEntityException("Could not save Tag");
                }
            }

            $this->_tagCache[$tagName] = $tag;

            return $tag;
        }
        return null;
    }

    protected function defaultTagNameNormalizer(string $tagName) : string
    {
        return preg_replace('/[^\w\d\- ]+/', '', trim(strtolower($tagName)));
    }

    protected function normalizeTagName(string $tagName) : string
    {
        $tagNameNormalizer = $this->getConfig('tagNameNormalizer');
        return is_callable($tagNameNormalizer) ? $tagNameNormalizer($tagName) : $this->defaultTagNameNormalizer($tagName);
    }
}