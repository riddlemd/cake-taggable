<?php
namespace Riddlemd\Taggable\Controller\Component;

use Cake\Controller\Component;
use Riddlemd\Tools\Core\Configure;
use Cake\ORM\TableRegistry;

class TaggableComponent extends Component
{
    public function hasAtLeastOne(\Cake\ORM\Query &$query, $table, $tags)
    {
        if(!$table->behaviors()->has('Taggable'))
            throw new \Exception('Table does not have Taggable Behavior!');

        $behavior = $table->behaviors()->get('Taggable');
        if(is_string($tags))
        {
            $tags = explode($behavior->config('delimiter'), $tags);
        }

        $query->matching('TagAssociations.Tags', function($q) use($tags) {
            return $q
                ->where([
                    'Tags.name in' => $tags
                ]);
        });
    }

    public function hasAll(\Cake\ORM\Query &$query, \Cake\ORM\Table $table, $tags)
    {

    }

    public function doesNotHave(\Cake\ORM\Query &$query, \Cake\ORM\Table $table, $tags)
    {

    }
}