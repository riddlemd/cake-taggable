<?php
namespace Riddlemd\Taggable\Exception;

use Cake\Core\Exception\Exception;

class CouldNotSaveEntityException extends Exception
{

}